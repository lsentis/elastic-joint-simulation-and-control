clear all;

Th_before = [0;0];
dTh_before = [0;0];
q_before = [0;0];
dq_before = [0;0];

T_motor = [10;10];%the poke

% servo loop
ii = 1;
inc_t = 0.001;
tt = 0;
while tt(end) < 5;

	[Th_vec(:,ii), dTh_vec(:,ii), q_vec(:,ii), dq_vec(:,ii), T_sensor_vec(:,ii)] =...
		robot_elastic_plant(inc_t, Th_before, dTh_before, 
		q_before, dq_before, T_motor);

	Th_before = Th_vec(:,end);
	dTh_before = dTh_vec(:,end);
	q_before = q_vec(:,end);
	dq_before = dq_vec(:,end);
	tt(ii) = tt(end) + inc_t;
	ii = ii+1;
end

figure(1)
clf;
subplot(2,2,1)
plot(tt,Th_vec(1,:),'linewidth',1);
hold on;
plot(tt,q_vec(1,:),'r','linewidth',1);
legend('Th1', 'q1');
xlabel('t[s]','fontsize',18);
ylabel('[rad]','fontsize',18);
subplot(2,2,2)
plot(tt,dTh_vec(1,:),'linewidth',1);
hold on;
plot(tt,dq_vec(1,:),'r','linewidth',1);
legend('dTh1','dq');
xlabel('t[s]','fontsize',18);
ylabel('[rad/s]','fontsize',18);
subplot(2,2,3)
plot(tt,T_sensor_vec(1,:),'linewidth',1);
hold on;
plot(tt,ones(1,size(tt,2))*T_motor(1,:),'r','linewidth',1);
legend('torque sensor 1','torque motor 1');
xlabel('t[s]','fontsize',18);
ylabel('[Nm]','fontsize',18);

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot1.jpg

figure(2)
clf;
subplot(2,2,1)
plot(tt,Th_vec(2,:),'linewidth',1);
hold on;
plot(tt,q_vec(2,:),'r','linewidth',1);
legend('Th2', 'q2');
xlabel('t[s]','fontsize',18);
ylabel('[rad]','fontsize',18);
subplot(2,2,2)
plot(tt,dTh_vec(2,:),'linewidth',1);
hold on;
plot(tt,dq_vec(2,:),'r','linewidth',1);
legend('dTh2','dq');
xlabel('t[s]','fontsize',18);
ylabel('[rad/s]','fontsize',18);
subplot(2,2,3)
plot(tt,T_sensor_vec(2,:),'linewidth',1);
hold on;
plot(tt,ones(1,size(tt,2))*T_motor(2,:),'r','linewidth',1);
legend('torque sensor 2','torque motor 2');
xlabel('t[s]','fontsize',18);
ylabel('[Nm]','fontsize',18);

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot2.jpg
