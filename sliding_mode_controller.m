function [uu, state] = sliding_mode_controller(q,dq,state)

% states
q0 = 0; % let's assume we start at zero
dq0 = 0;
q1 = 0.2;
dq1 = 0.5;
q2 = 1.8;
dq2 = dq1;
q3 = 2;
dq3 = 0;
ball_radius = 0.05;

% distances
d0 = sqrt( (q - q0)^2 + (dq - dq0)^2);
d1 = sqrt( (q - q1)^2 + (dq - dq1)^2);
d2 = sqrt( (q - q2)^2 + (dq - dq2)^2);
d3 = sqrt( (q - q3)^2 + (dq - dq3)^2);

% trajectory parameters
alpha0 = 0;
beta0 = dq1/10;%0.06
alpha1 = (dq1-dq0)/(q1-q0);%6
beta1 = dq1-alpha1*q1;%0
alpha2 = 0;
beta2 = dq1;%0.3
alpha3 = (dq3-dq2)/(q3-q2);%-1.5
beta3 = dq3-alpha3*q3;%1.5

% control gains
eta0 = 10.5;
eta1 = eta0;
eta2 = eta0;
eta3 = eta0;
gamma0 = 3;
gamma1 = gamma0;
gamma2 = gamma1;
gamma3 = gamma1;

% states
if d0 < ball_radius & state == -1
 	state = 0
elseif dq > beta0 & state == 0
	state = 1
elseif dq > beta2 & state == 1
  	state = 2
elseif q > q2 & state == 2
  	state = 3
elseif d3 < ball_radius & state == 3
	state = 4
end


% control laws
if state == 0
	s0 = dq - beta0;
	uu = 0 - eta0 * tanh(s0*gamma0);
elseif state == 1
	s1 = dq - alpha1 * q - beta1;
	uu = alpha1 - eta1 * tanh(s1*gamma1);
elseif state == 2
	s2 = dq - beta2;
	uu = 0 - eta2 * tanh(s2*gamma2);
elseif state == 3	
	s3 = dq - alpha3 * q - beta3;
	uu = alpha3 - eta3 * tanh(s3*gamma3);
elseif state == 4
	kp_position = 50;
	kd_position = 2*sqrt(kp_position);
	uu = -kp_position * (q-q3) - kd_position * (dq-dq3);
end