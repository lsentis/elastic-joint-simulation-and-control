% J2

clear all;

rotor_weight = 0.2;
rotor_radius = 0.05;
gear_ratio = 80;
stiffness = 4011;%Nm/rad
gear_friction = 40;%Nm/rad/s %Is not critically damped. it is underdamped

Theta_0 = 0;
dTheta_0 = 0;
Theta_before = Theta_0;
dTheta_before = dTheta_0;

kp = 100;%2*0.05*gear_ratio;% realistic values from Nic's controller
kd = 0.1;%*sqrt(kp);%interestingly, the differential value can be much smaller than critically damped

% servo loop
ii = 1;
inc_t = 0.001;
tt = 0;
Torque_sensor_vec = 0;
dTorque_sensor_vec = 0;
Torque_motor_vec = 0;
Torque_des_vec = 0;

while tt(end) < 2;

	Torque_des_vec(ii) = 10*sin(2*pi*50*tt(end));
	
	Torque_motor_vec(ii) = - kp * (Torque_sensor_vec(end) - Torque_des_vec(end)) - kd * dTorque_sensor_vec(end) ...
		+ Torque_des_vec(end);

	[Theta_vec(ii), dTheta_vec(ii), Torque_sensor_vec(ii)] = ...
		elastic_actuator_force_plant(inc_t, Theta_before, dTheta_before, ... 
		Torque_motor_vec(end), rotor_weight, rotor_radius, gear_ratio, ...
		gear_friction, stiffness);

	if ii > 1
		dTorque_sensor_vec(ii) = (Torque_sensor_vec(ii)-Torque_sensor_vec(ii-1))/inc_t;
	end

	Theta_before = Theta_vec(end);
	dTheta_before = dTheta_vec(end);
	tt(ii) = tt(end) + inc_t;
	ii = ii+1;
end

figure(2)
clf;
subplot(2,2,1)
plot(tt,Theta_vec,'linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('Theta [rad]','fontsize',18);
subplot(2,2,2)
plot(tt,dTheta_vec,'linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('dTheta [rad/s]','fontsize',18);
subplot(2,2,3)
plot(tt,Torque_sensor_vec,'linewidth',1);
hold on;
plot(tt,Torque_des_vec,'r','linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('Torque Sensor [Nm]','fontsize',18);
legend('Sensor','Desired');
subplot(2,2,4)
plot(tt,Torque_motor_vec,'linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('Torque Motor [Nm]','fontsize',18);

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot2.jpg