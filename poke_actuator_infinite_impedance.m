% J2

clear all;

% values of J2
rotor_weight = 0.1;
rotor_radius = 0.03;
gear_ratio = 160;
stiffness = 4000;%Nm/rad
gear_friction = 60;%Nm/rad/s %Is not critically damped. it is underdamped

% time constrant is about 0.5 secs. pretty low.

Theta_0 = 0;
dTheta_0 = 0;
Theta_before = Theta_0;
dTheta_before = dTheta_0;

Torque_motor = 1;

% servo loop
ii = 1;
inc_t = 0.001;
tt = 0;
while tt(end) < 5;

	[Theta_vec(ii), dTheta_vec(ii), Torque_sensor_vec(ii)] = ...
		elastic_actuator_force_plant(inc_t, Theta_before, dTheta_before, ... 
		Torque_motor, rotor_weight, rotor_radius, gear_ratio, ...
		gear_friction, stiffness);


	Theta_before = Theta_vec(end);
	dTheta_before = dTheta_vec(end);
	tt(ii) = tt(end) + inc_t;
	ii = ii+1;
end

figure(2)
clf;
subplot(3,1,1)
plot(tt,Theta_vec,'linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('Theta [rad]','fontsize',18);
subplot(3,1,2)
plot(tt,dTheta_vec,'linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('dTheta [rad/s]','fontsize',18);
subplot(3,1,3)
plot(tt,Torque_sensor_vec,'linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('Torque Sensor [Nm]','fontsize',18);

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot2.jpg