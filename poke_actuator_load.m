% J2

clear all;

grav = 10;

% values of J2
rotor_weight = 0.1;
rotor_radius = 0.03;
gear_ratio = 160;
stiffness = 4000;%Nm/rad
gear_friction = 60;%Nm/rad/s %Is not critically damped. it is underdamped
load_inertia=5;
mass = 17;
center_of_mass = 0.43;

% time constrant is about 0.5 secs. pretty low.

Theta_0 = 0;
dTheta_0 = 0;
Theta_before = Theta_0;
dTheta_before = dTheta_0;
q_0 = 0;
dq_0 = 0;
q_before = q_0;
dq_before = dq_0;

Torque_motor = 30;%the poke

% servo loop
ii = 1;
inc_t = 0.001;
tt = 0;
while tt(end) < 5;

	[Theta_vec(ii), dTheta_vec(ii), q_vec(ii), dq_vec(ii), Torque_sensor_vec(ii)] = ...
		elastic_actuator_position_plant(inc_t, Theta_before, dTheta_before, ... 
		q_before, dq_before, Torque_motor, rotor_weight, rotor_radius, gear_ratio, ...
		gear_friction, stiffness, load_inertia, mass, center_of_mass, grav);


	Theta_before = Theta_vec(end);
	dTheta_before = dTheta_vec(end);
	q_before = q_vec(end);
	dq_before = dq_vec(end);
	tt(ii) = tt(end) + inc_t;
	ii = ii+1;
end

figure(2)
clf;
subplot(2,2,1)
plot(tt,Theta_vec,'linewidth',1);
hold on;
plot(tt,q_vec,'r','linewidth',1);
legend('Theta', 'q');
xlabel('t[s]','fontsize',18);
ylabel('Theta [rad]','fontsize',18);
subplot(2,2,2)
plot(tt,dTheta_vec,'linewidth',1);
hold on;
plot(tt,dq_vec,'r','linewidth',1);
legend('dTheta','dq');
xlabel('t[s]','fontsize',18);
ylabel('dTheta [rad/s]','fontsize',18);
subplot(2,2,3)
plot(tt,Torque_sensor_vec,'linewidth',1);
hold on;
plot(tt,ones(1,size(tt,2))*Torque_motor,'r','linewidth',1);
legend('torque sensor','torque motor');
xlabel('t[s]','fontsize',18);
ylabel('Torque Sensor [Nm]','fontsize',18);

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot2.jpg