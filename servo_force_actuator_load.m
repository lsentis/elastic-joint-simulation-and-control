% J2
	
clear all;

grav = 10;%10;

% values of J2
rotor_weight = 0.2;
rotor_radius = 0.05;
gear_ratio = 80;
stiffness = 4011;%Nm/rad
gear_friction = 40;%Nm/rad/s %Is not critically damped. it is underdamped
load_inertia=5;%kgm^2
mass = 17;
center_of_mass = 0.43;

% time constrant is about 0.5 secs. pretty low.

Theta_0 = 0;
dTheta_0 = 0;
Theta_before = Theta_0;
dTheta_before = dTheta_0;
q_0 = 0;
dq_0 = 0;
q_before = q_0;
dq_before = dq_0;

% torque servo
kp_torque = 5;% realistic values from Nic's controller
kd_torque = 0.2;%*sqrt(kp_torque);%interestingly, the differential value can be much smaller than critically damped

% servo loop
ii = 2;
inc_t = 0.001;%0.001; 1ms servo loop
tt1 = 0;
Torque_sensor_vec = 0;
dTorque_sensor_vec = 0;
Torque_motor_vec = 0;
Torque_des_vec = 0;
q_vec = 0;
dq_vec = 0;

% position servo
jj = 2;
tt2 = 0;
kp_position = 50;%gain before inertial weighting
kd_position = 2*sqrt(kp_position);
qdes_vec = 0;
dqdes_vec = 0;
ee_vec = q_vec-qdes_vec;
dee_vec = dq_vec-dqdes_vec;
%u_vec = -kp_position * ee_vec - kd_position * dee_vec;
ff = 0.3;%hz frequency of tracking signal
u_vec = 0;

%velocity low pass filter
%tracking bandwidth

% user parameter - position frequency
position_servo = 10;%10;%ms [1-r]
position_count = 0;

% user parameter - position update - communication delay
delay = 20;%times servo loop
count_delay = 0;
state = -1;

% user parameter - controller choice - 0 PD - 1 Sliding Mode Control
controller_choice = 0;


while tt1(end) < 9;

	qdes_vec(ii) = 0.7*sin(2*pi*ff*tt1(end));
	dqdes_vec(ii) = 0.7*2*pi*ff*cos(2*pi*ff*tt1(end));

	% position loop (slower than torque loop)
	if position_count >= position_servo
		% pd controller
		if controller_choice == 0
			u_vec(jj) = -kp_position * ee_vec(end) - kd_position * dee_vec(end);
		% sliding mode controller 
		elseif controller_choice == 1
			[u_vec(jj), state] = sliding_mode_controller(q_vec(end), dq_vec(end), state);
		end

		% parameters
		tt2(jj) = tt2(end) + inc_t * position_servo;
		jj = jj+1;	
		position_count = 0;
	end
	position_count++;

	% torque trajectory
	%Torque_des_vec(ii) = 10*sin(2*pi*tt1(end));

	% gravity disturbance
	grav2 = grav * 0.8;

	% time delay 1ms per loop
	if count_delay < delay
		Torque_des_vec(ii) = 0;
		count_delay++;
	else
		Torque_des_vec(ii) = load_inertia * u_vec(end-delay/position_servo) +...
			mass * center_of_mass * grav2 * sin( q_vec(end) );
	end

	% torque loop
	Torque_motor_vec(ii) = - kp_torque * (Torque_sensor_vec(end) - Torque_des_vec(end)) - kd_torque * dTorque_sensor_vec(end) ...
		+ Torque_des_vec(end);

	%Torque_motor_vec(ii) = Torque_des_vec(end);%open loop torque

	% actuator plant
	[Theta_vec(ii), dTheta_vec(ii), q_vec(ii), dq_vec(ii), Torque_sensor_vec(ii)] = ...
		elastic_actuator_position_plant(inc_t, Theta_before, dTheta_before, ... 
		q_before, dq_before, Torque_motor_vec(end), rotor_weight, rotor_radius, gear_ratio, ...
		gear_friction, stiffness, load_inertia, mass, center_of_mass, grav);

	% add clogging torque
	% based on Nick measurements 1-2 cogs per motor revolution = 160-320 cogs per output turn
	%torque_noise_freq = 160;
	% Tq = 2*pi/320;% 
	% torque_noise_ampl = 0.4;
	% Torque_sensor_vec(end) += torque_noise_ampl * sin(2*pi/Tq*q_vec(end));

	% add noise to velocity sensor
	% noise_freq = 10;
	% if abs(dq_vec) > 0.1
	% 	noise_ampl = dq_vec(end) * 0.01;
	% else
	% 	noise_ampl = 0.005;
	% end
	% dq_vec(end) += rand(1) * noise_ampl * cos(2*pi*noise_freq*tt1(end));

	% position and velocity updates
	ee_vec(ii) = q_vec(end) - qdes_vec(end);
	dee_vec(ii) = dq_vec(end) - dqdes_vec(end);

	% torque derivative
	if ii > 1
		dTorque_sensor_vec(ii) = (Torque_sensor_vec(ii)-Torque_sensor_vec(ii-1))/inc_t;
	end

	Theta_before = Theta_vec(end);
	dTheta_before = dTheta_vec(end);
	q_before = q_vec(end);
	dq_before = dq_vec(end);
	tt1(ii) = tt1(end) + inc_t;
	ii = ii+1;
end

figure(1)
clf;
plot(q_vec,dq_vec,'linewidth',1);
xlabel('q[rad]','fontsize',18);
ylabel('dq[rad/s]','fontsize',18);
axis([-0.1 2.1 -0.1 1])

figure(2)
clf;
subplot(2,2,1)
plot(tt1,Theta_vec,'linewidth',1);
hold on;
plot(tt1,q_vec,'r','linewidth',1);
if controller_choice == 0
	plot(tt1,qdes_vec,'g','linewidth',1)
	legend('Theta','q','qdes');
else
	legend('Theta','q');
end
xlabel('t[s]','fontsize',18);
ylabel('[rad]','fontsize',18);
subplot(2,2,3)
plot(tt1,dTheta_vec,'linewidth',1);
hold on;
plot(tt1,dq_vec,'r','linewidth',1);
legend('dTheta','dq');
xlabel('t[s]','fontsize',18);
ylabel('[rad/s]','fontsize',18);
subplot(2,2,2)
plot(tt1,Torque_sensor_vec,'linewidth',1);
hold on;
plot(tt1,Torque_des_vec,'r','linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('[Nm]','fontsize',18);
legend('torque sensor','torque des');
subplot(2,2,4)
plot(tt1,Torque_motor_vec,'linewidth',1);
hold on;
plot(tt1,Torque_des_vec,'r','linewidth',1);
legend('torque motor','torque des');
xlabel('t[s]','fontsize',18);
ylabel('[Nm]','fontsize',18);
axis([tt1(1) tt1(end) -300 300])

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot2.jpg