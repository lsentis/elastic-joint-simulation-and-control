function dqhat = velocity_disturbance(dq,ttime,dist_ampl)

freq = 10;%hz

dqhat = dq + rand(1) * dist_ampl * sin(2*pi*freq*ttime(end));
