function Adiag = diagonal(A)

Adiag = zeros(size(A,1), size(A,2));
for ii=1:size(A,1)
	Adiag(ii,ii) = A(ii,ii);
end