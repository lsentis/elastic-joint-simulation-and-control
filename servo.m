% servo
clear all;

%hat = SCARA_estimated_plant();

% initial state
q0 = [0;0];
dq0 = [0;0];

% desired state
qdes = [1;1];
dqdes = [0;0];

% gains 
k_p = 100;
k_d = 20;

% current state
q = q0;
dq = dq0;
dqhat = dq0;

% control law
ee = q-qdes;
dee = dq-dqdes;

% servo loop
ii = 1;
inc_t = 0.01;
tt = 0;
while tt(end) < 5;
	[Ahat, Ghat] = SCARA_estimated_plant(q);
	%Ahat = mass_disturbance(Ahat, 50);	
	%Ghat = gravity_disturbance(Ghat, 30);	
	%Ahat = diagonal(Ahat);
	u = -k_p * ee - k_d * dqhat;
	Torque = Ahat * u + Ghat;
	% sim
	centrifugal_on = 0;
	[q dq ddq] = SCARA_plant(inc_t,q,dq,Torque,centrifugal_on);
	dqhat = velocity_disturbance(dq,tt,0.1); % noise on velocity signal
	ee = q-qdes;
	qvec(:,ii) = [q;qdes];
	error(:,ii) = ee;
	dqvec(:,ii)= [dq;dqhat];
	if ii > 1
		tt(ii) = tt(ii-1)+ inc_t;
	end
	ii = ii+1;
end

% plotting
figure(1)
clf;
subplot(2,1,1)
plot(tt,qvec(1,:),'linewidth',1);
hold on;
plot(tt,qvec(3,:),'r','linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('q1 [rad]','fontsize',18);
subplot(2,1,2)
plot(tt,qvec(2,:),'linewidth',1);
hold on;
plot(tt,qvec(4,:),'r','linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('q2 [rad]','fontsize',18);

% printing data
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperPosition',[0, 0, 7, 5]);
print -djpeg plot.jpg

figure(2)
clf;
subplot(2,1,1)
plot(tt,dqvec(1,:),'linewidth',1);
hold on;
plot(tt,dqvec(3,:),'r','linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('dq1 [rad/s]','fontsize',18);
subplot(2,1,2)
plot(tt,dqvec(2,:),'linewidth',1);
hold on;
plot(tt,dqvec(4,:),'r','linewidth',1);
xlabel('t[s]','fontsize',18);
ylabel('dq2 [rad/s]','fontsize',18);

% delays

% diagonal mass matrix

% 
