% elastic actuator position plant

function [Theta, dTheta, q, dq, Torque_sensor] = elastic_actuator_force_plant(inc_t, Theta_before, 
	dTheta_before, q_before, dq_before, Torque_motor, rotor_weight, rotor_radius, gear_ratio, 
	gear_friction, stiffness, load_inertia, mass, center_of_mass, grav)

JJ = rotor_weight * rotor_radius^2 / 2.0 * gear_ratio^2;
%JJ = 0.8;
KK = stiffness;
BB = gear_friction;
LL = load_inertia;

% J \ddot \theta + B \dot \theta + K (\theta -q)= Torque_motor
% L \ddot q + K(q-\theta) = 0
% K (\theta -q)= Torque_sensor

% motor accelerations
ddTheta = JJ^-1 * ( Torque_motor - BB*dTheta_before ...
	- KK*(Theta_before-q_before) );
dTheta = dTheta_before + ddTheta * inc_t;
Theta = Theta_before + dTheta_before * inc_t + 0.5 * ddTheta * inc_t^2;

% joint accelerations
ddq = LL^-1 * ( -KK * ( q_before - Theta_before )  - mass * center_of_mass * grav * sin(q_before) );
dq = dq_before + ddq * inc_t;
q = q_before + dq_before * inc_t + 0.5 * ddq * inc_t^2;

% torque sensor
Torque_sensor = KK * ( Theta - q );

