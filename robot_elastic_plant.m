% elastic actuator position plant

function [Th, dTh, q, dq, T_sensor] = robot_elastic_plant(inc_t, Th_before, dTh_before, 
	q_before, dq_before, T_motor)

% actuator parameters
rotor_inertia = 0.8;
stiffness = 4011;
gear_friction = 40;

JJ = eye(2) * rotor_inertia;
KK = eye(2) * stiffness;
BB = eye(2) * gear_friction;

% load parameters
l1 = 0.35;
l2 = 0.294;
L1 = 0.3;%com
L2 = 0.18;%com
m1 = 11.17;
m2 = 6.82;
I1 = 1;
I2 = 0.224;

A11 = m1*L1^2 + m2*l1^2 + m2*L2^2 + 2*m2*l1*L2*cos(q_before(2)) + I1 + I2;
A12 = A21 = m2*L2^2 + m2*l1*L2*cos(q_before(2)) + I2;	
A22 = m2*L2^2 + I2;
A = [A11 A12; A12 A22];

G1 = -10*m1*L1*cos(q_before(1)) - 10*m2*l1*cos(q_before(1)) - 10*m2*L2*cos(q_before(1)+q_before(2));
G2 = -10*m2*L2*cos(q_before(1)+q_before(2));
G = [G1;G2];

B1 = -2*L1*l2*m2*sin(q_before(2)) * dq_before(1) * dq_before(2) - L1*l2*m2*sin(q_before(2)) * dq_before(2)^2;
B2 = L1*l2*m2*sin(q_before(2))*dq_before(1)^2;
B = [B1;B2];

% J \ddot \theta + B \dot \theta + K (\theta -q)= Torque_motor
% A \ddot q + B + G + K(q-\theta) = 0
% K (\theta -q)= Torque_sensor

ddTh = JJ^-1 * ( T_motor - BB*dTh_before - KK*(Th_before-q_before) );
dTh = dTh_before + ddTh * inc_t;
Th = Th_before + dTh_before * inc_t + 0.5 * ddTh * inc_t^2;

ddq = A^-1 * ( -B - G - KK * ( q_before - Th ) );
dq = dq_before + ddq * inc_t;
q = q_before + dq_before * inc_t + 0.5 * ddq * inc_t^2;
	
T_sensor = KK * ( Th - q );

