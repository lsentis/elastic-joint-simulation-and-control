function [A,G] = SCARA_estimated_plant(q)

l1 = 0.35;
l2 = 0.294;
L1 = 0.3;%com
L2 = 0.18;%com
m1 = 11.17;
m2 = 6.82;
I1 = 1;
I2 = 0.224;

A11 = m1*L1^2 + m2*l1^2 + m2*L2^2 + 2*m2*l1*L2*cos(q(2)) + I1 + I2;

A12 = A21 = m2*L2^2 + m2*l1*L2*cos(q(2)) + I2;	

A22 = m2*L2^2 + I2;

A = [A11 A12; A12 A22];

G1 = -10*m1*L1*cos(q(1)) - 10*m2*l1*cos(q(1)) - 10*m2*L2*cos(q(1)+q(2));
G2 = -10*m2*L2*cos(q(1)+q(2));

G = [G1;G2];
