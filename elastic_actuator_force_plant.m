% elastic actuator force plant
% we assume gear ratio included in the transmission
function [Theta, dTheta, Torque_sensor] = elastic_actuator_force_plant(inc_t, Theta_before, dTheta_before, 
	Torque_motor, rotor_weight, rotor_radius, gear_ratio, gear_friction, stiffness)

JJ = rotor_weight * rotor_radius^2 / 2.0 * gear_ratio^2;
KK = stiffness;
BB = gear_friction;

% high impedance plant 
% J \ddot \theta + B \dot \theta + K \theta = Torque_motor
% K \theta = Torque_sensor

ddTheta = JJ^-1 * ( Torque_motor - BB*dTheta_before - KK*Theta_before);

% integration
dTheta = dTheta_before + ddTheta * inc_t;
Theta = Theta_before + dTheta_before * inc_t + 0.5 * ddTheta * inc_t^2;

Torque_sensor = KK * Theta;