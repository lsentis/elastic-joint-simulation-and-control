function [q dq ddq] = SCARA_plant(inc_t, q_before, dq_before, Torque, coriolis_yes)

l1 = 0.35;
l2 = 0.294;
L1 = 0.3;%com
L2 = 0.18;%com
m1 = 11.17;
m2 = 6.82;
I1 = 1;
I2 = 0.224;

A11 = m1*L1^2 + m2*l1^2 + m2*L2^2 + 2*m2*l1*L2*cos(q_before(2)) + I1 + I2;

A12 = A21 = m2*L2^2 + m2*l1*L2*cos(q_before(2)) + I2;	

A22 = m2*L2^2 + I2;

A = [A11 A12; A12 A22];

G1 = -10*m1*L1*cos(q_before(1)) - 10*m2*l1*cos(q_before(1)) - 10*m2*L2*cos(q_before(1)+q_before(2));
G2 = -10*m2*L2*cos(q_before(1)+q_before(2));

G = [G1;G2];

% centrifugal - from Khatib's notes
if coriolis_yes
	B1 = -2*L1*l2*m2*sin(q_before(2)) * dq_before(1) * dq_before(2) - L1*l2*m2*sin(q_before(2)) * dq_before(2)^2;
	B2 = L1*l2*m2*sin(q_before(2))*dq_before(1)^2;
	B = [B1;B2];
else
	B = [0;0];
end


ddq = A^-1 * ( Torque - G - B);

% integration
dq = dq_before + ddq * inc_t;
q = q_before + dq_before * inc_t + 0.5 * ddq * inc_t^2;