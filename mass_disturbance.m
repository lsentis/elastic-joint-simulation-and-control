% disturbance
function Ahat = mass_disturbance(A, percent)

Ahat = A * (1 + percent/100);