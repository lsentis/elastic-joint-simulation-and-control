% disturbance
function Ghat = gravity_disturbance(G, percent)

Ghat = G * (1 + percent/100);